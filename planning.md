Finish the RESTful
    - finish adding decorators to view functions

 In events views:
* [x] - api_show_conference
    ["GET", "PUT", "DELETE"]

 In attendees views:
* [x] - api_show_attendee
    ["GET", "PUT", "DELETE"]

 In presentation views:
* [x] - api_list_presentations
    ["GET", "POST"]
    default value for status on new presentations should be "SUBMITTED"
    instead of: presentation = Presentation.objects.create(**content)
    try: presentation = Presentation.create(**content)
    ```````````````````````````````````````````````````````````````````
        @classmethod
        def create(cls, **kwargs):
            kwargs["status"] = Status.objects.get(name="SUBMITTED")
            presentation = cls(**kwargs)
            presentation.save()
            return presentation
    ```````````````````````````````````````````````````````````````````
[x] api_show_presentations
    ["GET", "PUT", "DELETE"]





W7D4
* [x] install requests
* [x] pip freeze requirements.txt


* [x] Update for when you create Location to get url to a picture of that city
  
* [] add acls.py file in events app
  * [] add code that will make the http requests to pexel and weather
  * [x] at top of acls.py import:
        from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
        json
        requests

    use pexels using api key
  * [x] will need header for request
      * [x] Update Location model(?) to include "picture_url" 
        * [x] None/null for existing
        * [x] run migrations
      * [x] create function in acls.py 
        * [x] accepts parameters (city, state)
        * [x] header name = "Authorization"
        * [x] header value = "{API key}"
        * [x] create params dictionary with per_page and query
        * [x] add url
        * [x] define response
        * [x] define content
        * [x] try - return pphoto
        * [x] except - return None
      * [x] update encoder in views



      * https://learn-2.galvanize.com/cohorts/3352/blocks/1873/content_files/build/04-hello-acls/66-integrating-third-party-data.md#:~:text=request.%20Here%27s%20a-,link%20to%20the%20example,-that%20shows%20how
        * instead of "user-agent" will be "Authorization"
  * To use API key:
  * [x] in .gitignore file add events/keys.py near top
  * [x] create events/keys.py file and put pexel and weather api keys in

 

* [x] Update Conference details to get weather data for city
    use Geocoding API to convert city to Latitude and Longitude
    Find current weather data for latitude longitude using above
    * [x] add temp and description to content
    * [x] if no data "weather": null,

    * [x] create function in acls.py

* [x] Add Dockerfile
* [x] Add Dockerfile.dev

* [] Add accounts
* [] Add tests
* [] Styling
* [] Update location functionality