from lib2to3.pgen2.token import OP
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

# create function that gets information from pexels
#   assign response variable for the get request to the api
#   create dictionary that will hold the response
#   create another dictionary with key value pairs from dictionary created from response
#   return last dictionary


def get_photo_url(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }

    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    print("URL for", content["photos"][0]["src"]["original"])
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
        
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    
    params = {
        "q": city + "," + state + "," + "US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
    }

    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    parameters = {
        "lat": content[0]['lat'],
        "lon": content[0]['lon'],
        "appid": OPEN_WEATHER_API_KEY,
        "units": 'imperial'
    }

    wurl = "https://api.openweathermap.org/data/2.5/weather"
    wresponse = requests.get(url=wurl, params=parameters)
    weather = json.loads(wresponse.content)
    temp = weather['main']['temp']
    description = weather['weather'][0]['description']

    try:
        return {"temp": temp, "description": description}

    except (KeyError, IndexError):
        return {"weather": None}

